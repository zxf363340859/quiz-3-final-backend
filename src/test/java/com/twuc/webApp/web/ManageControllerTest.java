package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ManageControllerTest extends ApiTestBase {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_and_json_when_input_product_database_have_not() throws Exception {
        Product product = new Product("soda", 5.0, "瓶", "ym5.jpg");
        String serializeProduct = serialize(product);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void should_return_repeat_when_input_product_database_have() throws Exception {
        Product product = new Product("soda", 5.0, "瓶", "ym5.jpg");
        String serializeProduct = serialize(product);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        Product commonProduct = new Product("soda", 5.0, "瓶", "ym5.jpg");
        String serializeCommonProduct = serialize(product);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isConflict());
    }


}
