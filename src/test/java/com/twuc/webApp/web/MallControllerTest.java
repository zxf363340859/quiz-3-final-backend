package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.request.ProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MallControllerTest extends ApiTestBase {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_all_product_when_get_all() throws Exception {
        ProductRequest productSoda = new ProductRequest("soda", 5.0, "p", "ym5.jpg");
        String serializeProductSoda = serialize(productSoda);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON).content(serializeProductSoda))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        ProductRequest productCola = new ProductRequest("cola", 5.0, "瓶", "ym5.jpg");
        String serializeProductCola = serialize(productCola);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON).content(serializeProductCola))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[{\"id\":1,\"name\":\"soda\",\"price\":5.0,\"unit\":\"p\",\"url\":\"ym5.jpg\"},{\"id\":2,\"name\":\"cola\",\"price\":5.0,\"unit\":\"ç\u0093¶\",\"url\":\"ym5.jpg\"}]"));
    }
}
