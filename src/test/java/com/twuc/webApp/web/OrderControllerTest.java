package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.request.OrderRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest extends ApiTestBase {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_201_when_input_order() throws Exception {
        Product product = new Product("soda", 5.0, "瓶", "url");
        String serializeProduct = serialize(product);
        mockMvc.perform(post("/api/order").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isCreated());
    }

    @Test
    void should_return_all_orders_when_get_all() throws Exception {
        Product product = new Product("soda", 5.0, "瓶", "url");
        String serializeProduct = serialize(product);
        mockMvc.perform(post("/api/order").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isCreated());
        mockMvc.perform(post("/api/order").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/api/orders"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[{\"id\":1,\"quantity\":1,\"product\":{\"id\":1,\"name\":\"soda\",\"price\":5.0,\"unit\":\"ç\u0093¶\",\"url\":\"url\"}},{\"id\":2,\"quantity\":1,\"product\":{\"id\":2,\"name\":\"soda\",\"price\":5.0,\"unit\":\"ç\u0093¶\",\"url\":\"url\"}}]"));
    }

    @Test
    void should_return_200_and_have_not_order_when_delete_order() throws Exception {
        Product product = new Product("soda", 5.0, "瓶", "url");
        String serializeProduct = serialize(product);
        mockMvc.perform(post("/api/order").contentType(MediaType.APPLICATION_JSON).content(serializeProduct))
                .andExpect(status().isCreated());


        OrderRequest order = new OrderRequest(1L);
        String serializeOrder = serialize(order);
        mockMvc.perform(delete("/api/order").contentType(MediaType.APPLICATION_JSON).content(serializeOrder))
                .andExpect(status().isOk());


    }
}
