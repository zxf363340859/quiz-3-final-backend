package com.twuc.webApp.repo;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.UserOrder;
import com.twuc.webApp.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class UserOrderRepoTest extends ApiTestBase {

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    ProductRepo productRepo;

    @Test
    void should_save_and_find_order_when_input_order() {
        UserOrder userOrder = new UserOrder(1);
        Product product = new Product("soda", 5.0, "瓶", "url");
        Product savedProduct = productRepo.saveAndFlush(product);
        userOrder.setProduct(savedProduct);
        orderRepo.saveAndFlush(userOrder);

        Optional<UserOrder> orderOptional = orderRepo.findById(1L);

        assertEquals(userOrder, orderOptional.get());
    }

    @Test
    void should_return_200_when_delete_order() {
        UserOrder userOrder = new UserOrder(1);
        Product product = new Product("soda", 5.0, "瓶", "url");
        Product savedProduct = productRepo.saveAndFlush(product);
        userOrder.setProduct(savedProduct);
        UserOrder savedUserOrder = orderRepo.saveAndFlush(userOrder);

        orderRepo.deleteById(savedProduct.getId());
        orderRepo.flush();

        Optional<UserOrder> orderOptional = orderRepo.findById(savedUserOrder.getId());
        assertFalse(orderOptional.isPresent());

    }
}
