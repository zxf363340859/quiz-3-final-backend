package com.twuc.webApp.repo;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductRepoTest extends ApiTestBase {

    @Autowired
    ProductRepo productRepo;

    @Test
    void should_save_product() {
        Product product = productRepo.saveAndFlush(new Product("soda", 5.0, "瓶", "url"));

        Optional<Product> productOptional = productRepo.findById(1L);

        assertEquals(product, productOptional.get());
    }

    @Test
    void should_return_all_product() {
        Product productSoda = productRepo.saveAndFlush(new Product("soda", 5.0, "瓶", "url"));
        Product productCola = productRepo.saveAndFlush(new Product("cola", 5.0, "瓶", "url"));

        List<Product> productsOptional = productRepo.findAll();

        assertEquals(2, productsOptional.size());
    }
}
