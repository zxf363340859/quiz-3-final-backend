package com.twuc.webApp.request;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class ProductRequest {

    @NotNull
    private String name;

    @NotNull
    private Double price;

    @NotNull
    private String unit;

    @NotNull
    private String url;

    public ProductRequest() {
    }

    public ProductRequest(@NotNull String name, @NotNull Double price, @NotNull String unit, @NotNull String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
