package com.twuc.webApp.repo;

import com.twuc.webApp.entity.UserOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepo extends JpaRepository<UserOrder, Long> {
}
