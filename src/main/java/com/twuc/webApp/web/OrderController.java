package com.twuc.webApp.web;

import com.twuc.webApp.entity.Product;
import com.twuc.webApp.entity.UserOrder;
import com.twuc.webApp.repo.OrderRepo;
import com.twuc.webApp.request.OrderRequest;
import com.twuc.webApp.request.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class OrderController {

    @Autowired
    private OrderRepo orderRepo;

    @PostMapping("/api/order")
    public ResponseEntity addOrder(@RequestBody ProductRequest product) {
        UserOrder userOrder = new UserOrder();
        userOrder.setQuantity(1);
        userOrder.setProduct(new Product(product));
        orderRepo.saveAndFlush(userOrder);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/api/orders")
    public ResponseEntity<List<UserOrder>> getOrders() {
        List<UserOrder> orders = orderRepo.findAll();
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(orders);
    }

    @DeleteMapping("/api/order")
    public ResponseEntity deleteOrder(@RequestBody OrderRequest order) {
        System.out.println("order ================= id" + order.getId());
        orderRepo.deleteById(order.getId());
        return ResponseEntity.ok(null);
    }

}
