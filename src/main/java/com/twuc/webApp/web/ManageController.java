package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repo.ProductRepo;
import com.twuc.webApp.request.ProductRequest;
import com.twuc.webApp.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class ManageController {

    @Autowired
    private ProductRepo productRepo;

    @PostMapping("/api/product")
    public ResponseEntity<ProductResponse> addProduct(@RequestBody @Validated ProductRequest product) throws JsonProcessingException {
        Product findProduct = productRepo.findByName(product.getName());
        if(findProduct != null) {
            return  ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        productRepo.save(new Product(product));
        return  ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON).build();
    }
}
