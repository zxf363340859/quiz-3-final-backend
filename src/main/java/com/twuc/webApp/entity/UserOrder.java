package com.twuc.webApp.entity;

import javax.persistence.*;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class UserOrder {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;


    @Column
    private Integer quantity;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Product product;

    public UserOrder() {
    }

    public UserOrder(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserOrder userOrder = (UserOrder) o;
        return Objects.equals(quantity, userOrder.quantity) &&
                Objects.equals(product, userOrder.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, product);
    }
}
