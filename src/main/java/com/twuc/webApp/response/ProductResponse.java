package com.twuc.webApp.response;

public class ProductResponse {

    private String message;

    public ProductResponse() {
    }

    public ProductResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
