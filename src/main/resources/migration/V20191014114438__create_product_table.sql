create table product (
        id bigint not null AUTO_INCREMENT,
        price double not null,
        name varchar(128) not null,
        unit varchar(2) not null,
        url varchar(255) not null,
        primary key (id)
);


