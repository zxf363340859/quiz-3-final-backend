create table user_order (
        id bigint not null AUTO_INCREMENT,
        quantity integer,
        product_id bigint,
        primary key (id),
        foreign key (product_id) references product(id)
    )
